<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use App\Repository\UserRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserType;

/**
 * @Route("/api/user", name="api_user")
 */
class RestApiController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods= "GET")
     */
    public function UserFindAll(UserRepository $repo)
    {
        $user = $repo->findAll();
        return new JsonResponse($this->serializer->serialize($user, 'json'), JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Route("/register", methods= "POST")
     */
    public function register (Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){

        $user = new User;
        $form = $this->createForm(UserType::class, $user);

        $form-> submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()){
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            $manager->flush();

            return $this->json("", 201);
        }
            return $this->json($form->getErrors(true), 400);
    }

    // public function login(AuthenticationUtils $authenticationUtils)
    // {
    //     $error = $authenticationUtils->getLastAuthenticationError();
    //     //$lastUsername = $authenticationUtils->getLastUsername();

        
    //     return $this->render('user/login.html.twig', [
    //         'error' => $error,
    //         //'userName' => $lastUsername
    //     ]);
    //     return $this->redirectToRoute('profile_show_articles');
    // }
    // /**
    //  * @Route("/profile/update_user", name="update_user")
    //  */
    // public function updateUser(Request $request, UserPasswordEncoderInterface $encoder)
    // {

    //     $user = $this->getUser();

    //     if ($this->getUser() !== $user) {
    //         return new Response("You dont have permisson to change this credentials.", 401);
    //     }
    //     $form = $this->createForm(UserType::class, $user);

    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {

    //         $pass = $encoder->encodePassword($user, $user->getPassword());

    //         $user->setPassword($pass);

    //         $userData = $this->getDoctrine()->getManager();
    //         $userData->flush();

    //         return $this->redirectToRoute('login');
    //     }

    //     return $this->render("user/update-user.html.twig", [
    //         'form' => $form->createView()
    //     ]);
    // }
}
