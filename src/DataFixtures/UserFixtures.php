<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class UserFixtures extends Fixture
{
    private $encoder;
    public const USER_REFERENCE = 'user';

    private $faker;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->faker = Faker\Factory::create('en_EN');
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $user = new User();
            $user->setName($this->faker->name);
            $user->setPsudoName($this->faker->lastName);
            $user->setEmail($this->faker->email);
            $user->setPassword($this->encoder->encodePassword($user, '12345'));
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            $this->addReference(self::USER_REFERENCE . $i, $user);
        }

            $user = new User();
            $user->setName('admin');
            $user->setPsudoName('AdM1');
            $user->setEmail('admin@admin.com');
            $user->setPassword($this->encoder->encodePassword($user, 'admin'));
            $user->setRole('ROLE_ADMIN');
            $manager->persist($user);

            $user = new User();
            $user->setName('CHO');
            $user->setPsudoName('CHO');
            $user->setEmail('cho@cho.com');
            $user->setPassword($this->encoder->encodePassword($user, 'cho'));
            $user->setRole('ROLE_CHO');
            $manager->persist($user);
        

        $manager->flush();
    }
}
