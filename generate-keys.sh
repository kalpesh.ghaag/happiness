if [ ! -d "./config/jwt" ]; then
   mkdir ./config/jwt
fi
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -in config/jwt/private.pem -out config/jwt/private2.pem
rm config/jwt/private.pem
mv config/jwt/private2.pem config/jwt/private.pem
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
chmod 755 config/jwt -R
